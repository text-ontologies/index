# Ontology for indexes

## To do

* metadata: minimal number of characters to trigger the autocompleter
* maybe one property for any type of index, namely i:h, instead of i:date, i:word, i:headword, i:keyword?
* Number of main entries of an index.

## Syntax for generation of a full text index

* fulltext (title,body) with parser ngram
* https://jena.apache.org/documentation/query/text-query.html#text-dataset-assembler

## Resources

[Dictionary of Indexing Terms](https://dictionary.joanneburek.com/)

[Indexing. Concepts and theory](https://isko.org/cyclo/indexing)

[YAMATO : Yet Another More Advanced Top-level Ontology](https://hozo.jp/onto_library/upperOnto.htm)
